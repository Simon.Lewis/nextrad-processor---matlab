close all;
clear all;
tic;

%=====================================================
%                      SETUP
%=====================================================
%Reference Directory
[experi_folder,node0,node1,ref,params] = file_reader('C:\Users\Dell\Desktop\reference_dir\no_amps\');
%Radar Parameters

process_pulses = 10000;
numSamples = params.num_pri*params.range_samples*2;
ref_length = 1000;
num_blocks = 9%params.num_pri/process_pulses;
min_range = 800;
max_range = 2200;
%start_pulse = 100;
range_samples = params.range_samples;
PRF = floor(params.prf);
Fs = 90e6;
Ts = 1/Fs;
%Samples
doppler_samples=2048;

range_bin0=1873;
range_bin1=852;
%Frequency Correction
fixed_offset = -87.98%-33.58;
freq_offset = -87.98%-33.58;%5.664;%16.41;
fine_offset_removal = 0;
%RTI Dynamic Range
dr_on = 0;
dr_min = 0 ;
dr_max = 6e5;
dr_min1 = 0 ;
dr_max1 = 6e5;
%Blanking
range_blank = 500; 
%MDEV
mdev_on=0;
%DopplerVideo
doppler_vids = 0;


more=1;
fft_samples =2048;
adv = 4;
image_shift = [fft_samples/adv];

dyn_range = [0];


mdev_tic_on = 0;
stitch_on = 1;


%==========================================================
%                         MAIN
%=========================================================
disp(['PRF: ' num2str(PRF)]);
disp(['Number of Range Samples: ' num2str(range_samples)]); 
disp(['Frequency Band: ' params.band])
disp(['Pulse Length: ' num2str(params.pulse_len)])
disp(['CPI: ' num2str(1/PRF) 'us'])



fir = fopen(ref,'r');
Datar = fread(fir,3500,'int16');
fclose(fir);
D_Cr = Datar(1:2:end) + 1i*Datar(2:2:end);    %Reference Array
reference = D_Cr(1:ref_length); %Reference Length




num_pulses = process_pulses;
disp(['Number of Pulses: '  num2str(params.num_pri)]);
disp(['Recording Length: ' num2str(num_pulses*1/PRF) 's'])
disp(['Number of Matched Filter Blocks: ' num2str(num_blocks)])

N = floor(process_pulses*num_blocks/image_shift)-adv+1;
disp(['Number of Doppler Frames: ' num2str(N)]);

%'Frequency Band = ' params.band ...
fileID = fopen([experi_folder '\Parameters.txt'],'w');
fprintf(fileID,['Frequency Band = ' params.band ...
    '\r\nPRF = ' num2str(PRF) '\r\nRecording Length = ' ...
    num2str(num_pulses*1/params.prf) 's' ...
    '\r\nNumber of Pulses = '  num2str(num_pulses) ...
    '\r\nCPI = ' num2str(doppler_samples/PRF) ...
    '\r\nNumSample = ' num2str(numSamples) '\r\nRef_length = ' ...
     num2str(ref_length) '\r\nRangeSamples = ' num2str(range_samples) ...
     '\r\nSampleRate = ' num2str(Fs) ...
     '\r\nSamplePeriod = ' num2str(Ts) ...
     '\r\nDopplerSample = ' num2str(doppler_samples) ...
     '\r\nNode0RangeBin = ' num2str(range_bin0) ...
     '\r\nNode1RangeBin = ' num2str(range_bin1) ...
     '\r\nNode1FrequencyOffset = ' num2str(freq_offset) ...
     '\r\nStaticFOffset = ' num2str(fixed_offset) ...
     '\r\nAutoOffset = ' num2str(fine_offset_removal) ...
     '\r\nTurnOnDR = ' num2str(dr_on) ...
     '\r\nDynamicRangeMaxN0 = ' num2str(dr_max) ...
     '\r\nDynamicRangeMaxN1 = ' num2str(dr_max1)...
     '\r\nDynamicRangeMinN0 = ' num2str(dr_min) ...
     '\r\nDynamicRangeMinN1 = ' num2str(dr_min1)...
     '\r\nRangeBlank = ' num2str(range_blank)]);
fclose(fileID);

padded_reference = padarray(reference, (range_samples-length(reference)),'post');
clear D_Cr Datar
disp('Start ...')

% range_matrix0 = zeros(max_range-min_range+1,num_pulses*num_blocks);
% range_matrix1 = zeros(max_range-min_range+1,num_pulses*num_blocks);
% t_matrix = zeros(params.range_samples+1,num_pulses*num_blocks);

for i=1:num_blocks
    [r_matrix0,r_matrix1,t_matrix,block] = pulse_compression_matrix(i,node0,node1,params,Ts,num_pulses,padded_reference,fixed_offset,fine_offset_removal);
    range_matrix0(:,1+(i-1)*num_pulses:i*num_pulses)=r_matrix0(min_range:max_range,:);
    range_matrix1(:,1+(i-1)*num_pulses:i*num_pulses)=r_matrix1(min_range:max_range,:);
    time_matrix(:,1+(i-1)*num_pulses:i*num_pulses)=t_matrix(min_range:max_range,:);
    clear r_matrix0 r_matrix1 t_matrix
end

if fine_offset_removal
    disp('Removing Phase Slope ...');
    pulse_mag = abs(range_matrix1(range_bin1-min_range,:));
    pulse_ang_unwrap = unwrap(angle(range_matrix1(range_bin1-min_range,:)));
    phase_slope = polyfit(time_matrix(range_bin1-min_range,:),pulse_ang_unwrap,1);
    for n = 1:length(pulse_mag)
        pulse_ang(n) = pulse_ang_unwrap(n) - (phase_slope(1)*time_matrix(range_bin1-min_range,n)) - (phase_slope(2));
    end
    disp(['Phase Slope = ' num2str(phase_slope(1)) '*t + ' num2str(phase_slope(2))])
    a = angle(range_matrix1(range_bin1-min_range,:));
    b = unwrap(a);
    figure();
    title({'PC Down-Conversion Frequency Offset Removal',['Frequency Offset: ' num2str(phase_slope(1)/(2*pi)) ' Hz']});
    
    hold on;
    plot(a,'DisplayName', 'Original Wrapped Phase')
    plot(b,'DisplayName','Orignal Unwrapped Phase')
    plot(pulse_ang,'DisplayName','Phase Slope Removed')
    plot(phase_slope(1)*time_matrix(range_bin1-min_range,:) + (phase_slope(2)),'DisplayName','Linear Phase Fit')
    legend('show');
    range_matrix1(range_bin1-min_range,:) = pulse_mag.*exp(1i*pulse_ang);
   
end
    
    
    
 


x = min_range:max_range;
figure();
plot(x,abs(range_matrix0(:,10)),x,abs(range_matrix1(:,10)));
set(gca, 'YScale', 'log');
power_ratio=(abs(range_matrix0(range_bin0)))/(abs(range_matrix1(range_bin1)));
disp(['Power Ratio 0/1: ' num2str(power_ratio)]);


Fs = PRF;
figure();subplot(1,2,1);
[pxx,w]=periodogram(range_matrix0(range_bin0-min_range,1:doppler_samples),rectwin(length(range_matrix0(range_bin0-min_range,1:doppler_samples))),length(range_matrix0(range_bin0-min_range,1:doppler_samples)),Fs,'centered');
    npxx = pxx/max(pxx);
    plot(w, 10 * log10(npxx))
    xlim([-PRF/2 PRF/2])
    xlabel('normalized frequency   [rad / sample]')
    ylabel('normalized power spectral density')
    title ({['Bin ' num2str(range_bin0) ' Spectrum'];[num2str(doppler_samples) ' Samples']})
    grid on
subplot(1,2,2);
[pyy,u]=periodogram(range_matrix1(range_bin1-min_range,1:doppler_samples),rectwin(length(range_matrix1(range_bin1-min_range,1:doppler_samples))),length(range_matrix1(range_bin1-min_range,1:doppler_samples)),Fs,'centered');
    npyy = pyy/max(pyy);
    plot(u, 10 * log10(npyy))
    xlim([-PRF/2 PRF/2])
    xlabel('normalized frequency   [rad / sample]')
    ylabel('normalized power spectral density')
    title ({['Bin ' num2str(range_bin1) ' Spectrum'];[num2str(doppler_samples) ' Samples']})
    grid on


figure();
subplot(2,2,1)
plot(time_matrix(range_bin0-min_range,:), abs(range_matrix0(range_bin0-min_range,:)),time_matrix(range_bin0-min_range,:), power_ratio*abs(range_matrix1(range_bin0-min_range,:)),time_matrix(range_bin0-min_range,:),power_ratio*abs(range_matrix1(range_bin1-min_range,:)))
title('Magnitude vs Time')
subplot(2,2,2)
periodogram(abs(range_matrix0(range_bin0-min_range,:)) ...
    ,rectwin(length(abs(range_matrix0(range_bin0-min_range,:))))...
    ,length(abs(range_matrix0(range_bin0-min_range,:))),Fs,'centered');
subplot(2,2,3)
plot(time_matrix(range_bin1-min_range,:), abs(range_matrix1(range_bin1-min_range,:)))
title([range_bin1 'Magnitude'])
subplot(2,2,4)
periodogram(abs(range_matrix1(range_bin1-min_range,:)) ...    
    ,rectwin(length(abs(range_matrix1(range_bin1-min_range,:))))...
    ,length(abs(range_matrix1(range_bin1-min_range,:))),Fs,'centered');
title([range_bin1 'Amplitude Spectrum'])

figure();
subplot(2,2,1)
plot(time_matrix(range_bin0-min_range,:), unwrap(angle(range_matrix0(range_bin0-min_range,:))))
subplot(2,2,2)
periodogram(unwrap(angle(range_matrix0(range_bin0-min_range,:))) ...
    ,rectwin(length(abs(range_matrix0(range_bin0-min_range,:))))...
    ,length(angle(range_matrix0(range_bin0-min_range,:))),Fs,'centered');
subplot(2,2,3)
plot(time_matrix(range_bin1-min_range,:), unwrap(angle(range_matrix1(range_bin1-min_range,:))))
subplot(2,2,4)
periodogram(unwrap(angle(range_matrix1(range_bin1-min_range,:))) ...
    ,rectwin(length(abs(range_matrix1(range_bin1-min_range,:))))...
    ,length(abs(range_matrix1(range_bin1-min_range,:))),Fs,'centered');




figure()
subplot(1,2,1)
colormap 'jet';
    if dr_on        
        rti = imagesc([0 (num_pulses*1/PRF)],[0 range_samples],(abs(range_matrix0)),[dr_min dr_max]);
    else
         rti = imagesc([time_matrix(1,1) time_matrix(end,end)],[min_range max_range],(abs(range_matrix0-min_range)));
    end
    colorbar;
    title('RTI');
    xlabel('Time [s]');
    ylabel('Range Bin');
    set(gca,'YDir','normal');
subplot(1,2,2)
colormap 'jet';
    if dr_on        
        rti = imagesc([0 (num_pulses*1/PRF)],[0 range_samples],(abs(range_matrix1)),[dr_min1 dr_max1]);
    else
         rti = imagesc([time_matrix(1,1) time_matrix(end,end)],[min_range max_range],(abs(range_matrix1-min_range)));
    end
    colorbar;
    title('RTI');
    xlabel('Time [s]');
    ylabel('Range Bin');
    set(gca,'YDir','normal');


%MDEV
if mdev_on
    struct1=struct('rate',PRF,'phase',res);
    taus = 0.01:0.001:200;
    allan_overlap(struct1,taus);
end

range_struct=struct('node0',range_matrix0,'node1',range_matrix1);

if doppler_vids
    mkdir([experi_folder '\Doppler_Vid_' num2str(fft_samples)]);
    doppler_video(more,range_struct,fft_samples,PRF,N,image_shift,dyn_range,[1 300],[experi_folder '\Doppler_Vid_' num2str(fft_samples) '\'],min_range,max_range);
end

if mdev_tic_on
    taus = 1:1:1000;
    [fracfreqstruct,datastruct,data,sm,sme,tau] =mdev_calc(stitch_on,[experi_folder '\mdev.bin'],1,0,100e6,taus,'Test',1,0);
end

%=================================================
%                      END
%=================================================
toc;
disp('Complete.')

