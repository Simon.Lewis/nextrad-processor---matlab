function [pxx] = doppler_mat(more,range_matrix,fft_samples,prf,dyn_range,range_lims,imnum,direct,t,min_range,max_range)

  close(gcf);
  
  fig_handle=figure('Position', [0 0 1980 1020]);
  title([num2str(t) 's'])
  if more
      temp_m1 = (range_matrix.node0(1:end,1:fft_samples))';
      temp_m2 = (range_matrix.node1(1:end,1:fft_samples))';
  
      pxx=periodogram((temp_m1),rectwin(fft_samples),fft_samples,prf,'centered');
      pyy=periodogram((temp_m2),rectwin(fft_samples),fft_samples,prf,'centered');
      if dyn_range == 0 
        subplot(1,2,1)
        im=imagesc([min_range max_range],[-prf/2 prf/2],log10(pxx));
        colormap jet; 
        title([num2str(t) 's'])
        xlabel('Range Bin');
        ylabel('Doppler Frequency [Hz]');
        subplot(1,2,2)
        im2=imagesc([min_range max_range],[-prf/2 prf/2],log10(pyy));
        colormap jet;
        title([num2str(t) 's'])
        xlabel('Range Bin');
        ylabel('Doppler Frequency [Hz]');
      else
        subplot(1,2,1)  
        im=imagesc([min_range max_range],[-prf/2 prf/2],log10(pxx),[dyn_range(1) dyn_range(2)]);
        colormap jet; 
        subplot(1,2,2)
        title([num2str(t) 's'])
        xlabel('Range Bin');
        ylabel('Doppler Frequency [Hz]');
        im2=imagesc([range_lims(1) range_lims(2)],[-prf/2 prf/2],log10(pyy),[dyn_range(1) dyn_range(2)]);
        colormap jet; 
        title([num2str(t) 's'])
        xlabel('Range Bin');
        ylabel('Doppler Frequency [Hz]');
      end  
      
  else
      temp_m1 = (range_matrix(1:end,1:fft_samples))';
      pxx=periodogram((temp_m1),rectwin(fft_samples),fft_samples,prf,'centered');
      if dyn_range == 0 
        im=imagesc([range_lims(1) range_lims(2)],[-prf/2 prf/2],log10(pxx));
        colormap jet; 
        title([num2str(t) 's'])
        xlabel('Range Bin');
        ylabel('Doppler Frequency [Hz]');
      else
        im=imagesc([range_lims(1) range_lims(2)],[-prf/2 prf/2],log10(pxx),[dyn_range(1) dyn_range(2)]);
        colormap jet; 
        title([num2str(t) 's'])
        xlabel('Range Bin');
        ylabel('Doppler Frequency [Hz]');
      end  
      
  end
    
  %colorbar;  
  
  saveas(fig_handle,[direct num2str(imnum)],'png')
  % THE TIME SAVING IS A HACK AT THE MO! ASSUMES NON_OVERLAPPED SPECTRA d
  % surf(pxx);
  
  
end