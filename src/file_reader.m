 function [experi_folder, node0dir,node1dir,referncedir,params] = file_reader(reference_directory)
    
experi_folder = uigetdir;
    nextrad_ini_location = dir([experi_folder '\*_n0' '\NeXtRAD.ini']);
    disp('Reading Header File');

    fid = fopen([nextrad_ini_location.folder '\' nextrad_ini_location.name],'r');
    while 1
        tline = fgetl(fid);
    
         if strfind(tline, 'NUM_PRIS =')>0
            num_pri = str2double(tline(12:end));
        end
         
        
        if strfind(tline, 'SAMPLES_PER_PRI =')>0
            range_samples = str2double(tline(19:end));
        end
         
         if strfind(tline, 'WAVEFORM_INDEX = ')>0
            ref_index = str2double(tline(18:end));
        end
        
        if strfind(tline, 'PULSES = ')>0
            s = (tline(11:end));
            
        end
        
        if strfind(tline, 'ADC_CHANNEL = ')>0
            adc_chan = str2double((tline(15:end)));          
        end
        
        if ~ischar(tline)
           break
        end
    end
    count=0;
    for i = 1:length(s)
        if (s(i)==',') && count == 2
            mode = str2double(s(i-1));
            count = count + 1;
        end
        if (s(i)==',') && count == 1
            pri = str2double(s(in+1:i-1));
            count = count + 1;
        end
        if (s(i)==',') && count == 0
            pulse_length = str2double(s(1:i-1));
            in = i;
            count = count + 1;
        end
    end
    
    temp_fix =1;
    if temp_fix 
         if ref_index == 1
         pulse_length = 0.5;
     end
      if ref_index == 2
         pulse_length = 1;
     end
        if ref_index == 3
         pulse_length = 3;
     end 
    if ref_index == 4
         pulse_length = 5;
         
     end
     if ref_index == 5
         pulse_length = 10;
     end
     
    end
    
    if (mode<4) 
        referncedir = [reference_directory 'L' num2str(pulse_length) '.dat'];
        a = dir([experi_folder '\*_n0\*0.dat' ]);
        node0dir = [a.folder '\' a.name];
        b = dir([experi_folder '\*_n1\*0.dat' ]);
        node1dir = [b.folder '\' b.name];
        band = 'L';
    end
    
    if (mode==4) 
        referncedir = [reference_directory 'X' num2str(pulse_length) '.dat'];
        a = dir([experi_folder '\*_n0\*1.dat' ]);
        node0dir = [a.folder '\' a.name];
        b = dir([experi_folder '\*_n1\*1.dat' ]);
        node1dir = [b.folder '\' b.name];
        band = 'X';
    end
       
    if (mode==5) 
        referncedir = [reference_directory 'X' num2str(pulse_length) '.dat'];
        a = dir([experi_folder '\*_n0\*2.dat' ]);
        node0dir = [a.folder '\' a.name];
        b = dir([experi_folder '\*_n1\*2.dat' ]);
        node1dir = [b.folder '\' b.name];
        band = 'X';
    end 
    
%     if (adc_chan == 1) 
%         referncedir = [reference_directory 'X' num2str(pulse_length) '.dat'];
%         a = dir([experi_folder '\*_n0\*1.dat' ]);
%         node0dir = [a.folder '\' a.name];
%         b = dir([experi_folder '\*_n1\*1.dat' ]);
%         node1dir = [b.folder '\' b.name];
%         band = 'X';
%     end
%     if (adc_chan == 2) 
%         referncedir = [reference_directory 'X' num2str(pulse_length) '.dat'];
%         a = dir([experi_folder '\*_n0\*2.dat' ]);
%         node0dir = [a.folder '\' a.name];
%         b = dir([experi_folder '\*_n1\*2.dat' ]);
%         node1dir = [b.folder '\' b.name];
%         band = 'X';
%     end
%     if (adc_chan == 0)
%         referncedir = [reference_directory 'L' num2str(pulse_length) '.dat'];
%         a = dir([experi_folder '\*_n0\*0.dat' ]);
%         node0dir = [a.folder '\' a.name];
%         b = dir([experi_folder '\*_n1\*0.dat' ]);
%         node1dir = [b.folder '\' b.name];
%         band = 'L';
%     end
    pri = pri*1e-6;
    prf = 1/pri;
    
       
    
    params = struct('num_pri',num_pri,'mode',mode,'pri',pri,'prf',prf,'range_samples',range_samples,'pulse_len',pulse_length,'band',band);
    
end