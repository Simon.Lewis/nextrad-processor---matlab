
function doppler_video(more,range_matrix,fft_samples,prf,N,image_shift,dyn_range,range_lims,direct,min_range,max_range)
    close all
    tic;
    k = fft_samples/prf;
    t = zeros(1,N);
    t(1) = k;
   if more
    for i=1:N
       if i > 1
            t(i) = t(i-1) + (image_shift/fft_samples)*k;
       end
       rm1 = range_matrix.node0(range_lims(1):range_lims(2),((i-1)*image_shift+1:(i-1)*image_shift+fft_samples));
       rm2 = range_matrix.node1(range_lims(1):range_lims(2),((i-1)*image_shift+1:(i-1)*image_shift+fft_samples));
       
       rm = struct('node0',rm1,'node1',rm2);
       doppler_mat(more,rm,fft_samples,prf,dyn_range,range_lims,i,direct,t(i),min_range,max_range);
        
       toc;
    end
    
   else
        for i=1:N
            if i > 1
            t(i) = t(i-1) + (image_shift/fft_samples)*k;
            end
       rm = range_matrix(range_lims(1):range_lims(2),((i-1)*image_shift+1:(i-1)*image_shift+fft_samples));
       doppler_mat(more,rm,fft_samples,prf,dyn_range,range_lims,i,direct);
       toc;
        end 
   end
    
    toc;
end