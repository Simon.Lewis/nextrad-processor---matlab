function [range_matrix0, range_matrix1, time_matrix, block] = pulse_compression_matrix(block,node0,node1,params,Ts, num_pulses, padded_reference, fixed_offset,fine_offset_removal)

start_pulse = (block-1)*num_pulses;

fid0 = fopen(node0,'r');
status = fseek(fid0,(start_pulse)*4*params.range_samples,'bof'); 
Data0 = fread(fid0,num_pulses*params.range_samples*2,'int16');
fclose(fid0);
D_C0 = Data0(1:2:end) + 1i*Data0(2:2:end);    %Data node0 Array
clear Data0

fid1 = fopen(node1,'r');
status = fseek(fid1,(start_pulse)*4*params.range_samples,'bof'); 
Data1 = fread(fid1,num_pulses*params.range_samples*2,'int16');
fclose(fid1);

D_C1 = Data1(1:2:end) + 1i*Data1(2:2:end);    %Data node0 Array
clear Data1

throw_away_samples = mod(numel(D_C0),params.range_samples); %Delete Extra samples
D_C0 = D_C0(1:end-throw_away_samples);
D_C1 = D_C1(1:end-throw_away_samples);

t=zeros(1,length(D_C0));
t(1) = Ts+((block-1)*num_pulses/params.prf);
n=1;
for i=2:length(t)
    t(i) = t(i-1)+Ts;
    if mod(i,n*params.range_samples+1)==0
        t(i) = t(i-params.range_samples) + 1/params.prf;
        n = n+1;
    end
end

if fixed_offset~=0

    disp('Removing Offset ...');
    z = abs(D_C1).*exp(1i*angle(D_C1));
    D_C1 = z.*exp(1i*(-2*pi*fixed_offset*t')); 
    toc;
end

time_matrix =  reshape(t,params.range_samples,num_pulses);
raw_matrix0 = reshape(D_C0,params.range_samples,num_pulses);
raw_matrix1 = reshape(D_C1,params.range_samples,num_pulses);

range_matrix0 = zeros(2*params.range_samples-1,num_pulses);
range_matrix1 = zeros(2*params.range_samples-1,num_pulses);

clear D_C0 D_C1;

disp(['Cross-Correlation Started: Block ' num2str(block)]);
for j = 1:num_pulses
    range_matrix0(:,j) = xcorr(raw_matrix0(:,j),(padded_reference),'unbiased');
    range_matrix1(:,j) = xcorr(raw_matrix1(:,j),(padded_reference),'unbiased');

end

range_matrix0 = range_matrix0(params.range_samples:end,:);
range_matrix1 = range_matrix1(params.range_samples:end,:);

toc;


end